# Shaper Origin

## Spécifications

* Profondeur de découpe max. : 43 mm
* Diamètre du collet : 8 mm ou 1/8"
* Format de fichier supporté : SVG

Pour plus d'informations, voir la [fiche technique de Shaper Tools](https://www.shapertools.com/fr-fr/origin/spec) et le [guide d'utilisation](https://assets.shapertools.com/manual/Shaper_Origin_Product_Manual.pdf) (pdf en anglais et français).

## Précautions d'usage

* Utiliser la machine sur un plan de travail stable
* Pour la découpe : fixer le matériau sur le plan de travail
* Toujours relever la fraiseuse (bouton rouge) avant de passer d'une forme à une autre
* Toujours utiliser un aspirateur de poussières

![](/img/work.png)

![](/img/glue.png)

![](/img/clamp.png)

## Tutoriels

Shaper Tools met à disposition de nombreux [tutoriels vidéo](https://www.shapertools.com/fr-fr/tutorials) (en anglais).

### Pour débuter

Regarder [la leçon 2 : aircut](https://www.youtube.com/watch?v=sdsMOPZFMRQ) et [la leçon 3 : premier fraisage](https://www.youtube.com/watch?v=DekAjAOIVvQ).

### Pour comprendre les réglages

Les différents types de découpe :

[![](/img/type.jpg)](https://www.youtube.com/watch?v=B0O-0ejP-TQ)

Choisir la vitesse de fraisage :

[![](/img/speed.jpg)](https://www.youtube.com/watch?v=MEk43U8mf7g)

Pourquoi faire plusieurs passes ?

[![](/img/finish.jpg)](https://www.youtube.com/watch?v=qxoSaDLJRTY)

Pourquoi utiliser l'option offset ?

[![](/img/dogbone.jpg)](https://www.youtube.com/watch?v=sLo0yUkkO3M)

[![](/img/offset.jpg)](https://www.youtube.com/watch?v=ESnE2bEvMg0)

## Infos pratiques

![](/img/overview.png)

### Changer de fraise

![](/img/collet.png)

### Placement du ShaperTape

![](/img/tape_0.png) ![](/img/tape_1.png)

![](/img/tape_2.png) ![](/img/tape_3.png)

![](/img/tape_4.png) ![](/img/tape_5.png)

### Liens

* [Contrôle de la vitesse et du déplacement ](https://support.shapertools.com/hc/fr-fr/articles/360003345033-Contr%C3%B4le-de-la-vitesse-et-du-d%C3%A9placement)
* [Recommandations de paramètres par matériau](https://support.shapertools.com/hc/fr-fr/articles/360016398434-Recommandations-de-param%C3%A8tres-par-mat%C3%A9riau)
* [Paramètres de vitesse de la broche](https://support.shapertools.com/hc/fr-fr/articles/115003084574-Param%C3%A8tres-de-vitesse-de-la-broche)
