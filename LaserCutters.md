# Laser Cutters

## Précautions indispensables

### Pour éviter tout risque d'incendie

* Connaître avec certitude quel matériau est découpé
* Toujours activer l’air comprimé !
* Toujours allumer l’extracteur de fumée !
* Savoir où est le bouton d’arrêt d’urgence
* Savoir où trouver un extincteur au CO2
* Rester à proximité de la machine jusqu'à la fin de la découpe

### Pour votre santé

* Ne pas regarder fixement l'impact du faisceau LASER
* Après une découpe, ne pas ouvrir la machine tant qu'il y a de la fumée à l'intérieur

## Matériaux pour la découpe

### Matériaux recommandés

* Bois contreplaqué (plywood / multiplex)
* Acrylique (PMMA / Plexiglass)
* Papier, carton
* Textiles

### Matériaux déconseillés

* MDF : fumée épaisse, et très nocive à long terme
* ABS, PS : fond facilement, fumée nocive
* PE, PET, PP : fond facilement
* Composites à base de fibres : poussières très nocives
* Métaux : impossible à découper

### Matériaux interdits !

* PVC : fumée acide, et très nocive
* Cuivre : réfléchit totalement le LASER
* Téflon (PTFE) : fumée acide, et très nocive
* Résine phénolique, époxy : fumée très nocive
* Vinyl, simili-cuir : peut contenir de la chlorine
* Cuir animal : juste à cause de l'odeur

# Epilog Fusion Pro 32

## Spécifications

* Surface de découpe : 81 x 50 cm
* Hauteur maximum : 31 cm
* Puissance du LASER : 60 W
* Type de LASER : Tube CO2 (infrarouge)

## Manuel

[Manuel de l'Epilog Fusion Pro, comment envoyer un dessin vers la machine](EpilogFusion.md)

# Lasersaur

## Spécifications

* Surface de découpe : 122 x 61 cm
* Hauteur maximum : 12 cm
* Vitesse maximum : 6000 mm/min
* Puissance du LASER : 100 W
* Type de LASER : Tube CO2 (infrarouge)

## Driveboard App

Driveboard App est l'interface de contrôle de la machine.

Les formats de fichier compatibles sont DXF, SVG et DBA.

### DXF :

Difficile d'obtenir un DXF compatible avec Driveboard App.
Il existe différentes "versions" du DXF, choisissez la R14 si possible.

Pour exporter un DXF depuis Rhino, suivez [cet exemple sur le wiki Lasersaur](https://github.com/nortd/lasersaur/wiki/dxf_import).

En cas de problème, essayez d'importer le DXF dans Inkscape pour le sauver en SVG.
En cas de problème d'échelle, vérifier que le DXF a bien été exporté en milimètres (mm).

### SVG :

Permet de faire du color mapping, seuleument avec des couleurs RGB.

Si les couleurs n'apparaissent pas, ouvrir le SVG avec Inkscape et dégrouper tous les groupes.

En cas de problème d'échelle, ouvrir le SVG avec Inkscape et changer les unités du document en milimètres (mm).

### DBA

C'est le format de fichier spécifique à la Lasersaur. Il permet de sauver les réglages vitesse/puissance.

Très pratique pour le partage de fichiers avec color mapping, par exemple cette grille de test : [calibration_grid.dba](files/calibration_grid.dba)

## Manuel

[Manuel de la Lasersaur, un petit guide étape par étape pour ne rien oublier](Lasersaur.md)

# Full Spectrum Muse

## Spécifications

* Surface de découpe : 50 x 30 cm
* Hauteur maximum : 6 cm
* Puissance du LASER : 40 W
* Type de LASER : Tube CO2 (infrarouge) + pointeur rouge

## Retina Engrave

Retina Engrave est l'interface de contrôle de la machine.

Pour y accéder, connectez votre pc à la machine par cable ethernet.
Si votre portable n'a pas de prise ethernet, utilisez le routeur wifi dédié.

* Wifi : LaserCutter
* MDP : fablabULB2019

Une fois la connexion établie, entrez l'adresse http://fsl.local dans votre navigateur.

Formats de fichier pris en charge :

* Vectoriel : SVG → Le fichier sera importé en vectoriel mais aussi en
matriciel, il faudra donc en supprimer un des deux
* Matriciel : BMP, JPEG, PNG, TIFF
* PDF (vectoriel et matriciel)

Pour apprendre à utiliser Retina Engrave, suivez les [tutoriels vidéo](https://www.youtube.com/playlist?list=PL_1I1UNQ4oGa0w55C772Y1mC6F4f3ZcG6) (en anglais).

Pour les réglages, consultez [le site de Full Spectrum Laser](http://laser101.fslaser.com/materialtest).

## Ressources additionnelles

* [Laser Cutting tutorial by Massimo Menichinelli](https://www.slideshare.net/openp2pdesign/fab-academy-2015-laser-cutting)

## Credits
Axel Cornu
