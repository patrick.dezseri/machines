# IMPRESSION 3D : Comment préparer son G-code ? 

 
## Qu’est ce que c’est ? Comment sont produites les imprimantes 3D ?

* **Une impression 3D**

L'impression 3D regroupe les procédés de fabrication de pièces en volume par ajout de matière, fonctionnant de manière additive, par empilement de couches successives. 

* **Les imprimantes Prusa**

Les **machines Prusa** sont des imprimantes **imprimées par des imprimantes**, on appelle ça des fermes. Les pièces sont en open source. Par exemple, si une pièce de la machine viendrait à casser où à s’user on peut en imprimer une neuve.
Vous pouvez cliquez sur le lien suivant pour voir la ferme Prusa en action : [c’est par ici](https://www.youtube.com/watch?v=qqQzTvvrXo8).

## Précaution à prendre : 

* **Savoir si ce que nous voulons imprimer nécéssite l'utilisation de l'imprimante 3D** et que c'est la meilleure méthode pour réaliser son "objet".

Exemples de choses à ne pas imprimer : **boites** (peuvent être réalisées plus facilement et plus rapidement avec la découpeuse laser), imprimer une **bobine**,... 


## Spécifications de la machine : (Prusa I3MK3S)

Ces caractéristiques sont importantes et à prendre en compte en modélisant sa pièce en 3D.
-	Taille du plateau d’impression : **22,5cm x 22,5cm**
-	Hauteur d’impression maximale : **25 cm**
-	Diamètre de buse* (au FabLab ULB) : **0,4mm****

Une caractéristique importante de la Prusa est qu’elle a l’avantage de **se déplacer sur les axes X, Y et Z.**

*Il est important de connaitre le diamètre de la buse pour bien paramétrer la hauteur de couche des impressions.
**Par facilité nous utilisons des buses identiques sur toutes les machines.

# Préparation du fichier ?

### Logiciel utilisé pour générer son G-code : le slicer de Prusa

* Le **Slicer de Prusa**, vous pouvez le télécharger en cliquant sur ce lien. [Téléchargeable ici](https://www.prusa3d.com/drivers/)
* Les **formats de fichier** reconnus par le Slicer sont les fichiers  **.stl et .obj**


### Comment lancer une impression sur le plateau ?

A la première ouverture du Slicer de Prusa, nous vous conseillons de bien vous mettre en **mode « Expert »** , ce mode vous permettra de paramétrer votre impression de manière plus pointue. 

**ETAPE 1 : LE PLATEAU**

Une fois qui vous avez **importé votre objet 3D sur le plateau** (l’importation se fait soit en glissant votre fichier directement sur le plateau, soit en faisant Fichier > Importer > Importer STL/OBJ/…), il est important de bien **orienter votre modèle.** Il est plus intéressant d’imprimer une pièce sur sa base la plus large, par exemple. Afin de bien orienter l’objet, vous pouvez utiliser la palette d’outils qui se trouve sur la gauche de votre écran. Par exemple, sur l'image ci-dessous, il était plus judicieux d'imprimer les crochets sur leurs tranches, plutôt qu'à la verticale. 

Si vous avez plusieurs objets sur votre plateau, où que vous voulez en copier/coller un, les outils situés dans la barre supérieur vous permettent de le faire assez facilement. 

**Choisissez également le bon modèle d’imprimante** dans les onglets sur la droite de l’écran. 

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Module%203/prusa_import-min.png)

**ETAPE 2 : REGLAGES D’IMPRESSION**

Entrons dans le vif du sujet… 
Vous pouvez trouver les **réglages d’impressions** en haut de la fenêtre du Slicer.

1. _Couches et périmètres_ 

La **première chose** à faire quand vous paramétrez votre impression est de **régler la hauteur de couche**. La hauteur de couche est la hauteur à laquelle le plastique extrudé par la machine va s’écraser sur le plateau. Sa **hauteur/épaisseur** correspond à la **moitié de la hauteur de buse.** (Rappel : Il est donc important de ne pas négliger la dimension de la buse et de **connaitre les spécifications de la machine**.) 


2. _Les parois et coques_ 

Définissez l’**épaisseur de vos coques**. Un objet imprimé en 3D n’est **pas un objet plein**, il est constitué d’une coque externe et d’un remplissage. Définissez le nombre de couches qui définiront votre objet, il y’a les coques **verticales** et les coques **horizontales**. (Pour un usage qui ne nécessite pas trop de contraintes mécaniques trois couches d'épaisseur sont suffisantes, tant pour les verticales que les horizontales. 

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/clementine.benyakhou/-/raw/master/docs/images/capt1.JPG)

3. _Remplissage_

   * Définissez la **densité de remplissage**, vous déterminez la densité de remplissage en **fonction des usages** que vous ferez de votre pièce. Si une pièce doit être résistante celle-ci devra être la plus dense possible. Si votre objet a des fonctions décoratives, le remplissage peut être le plus léger possible. Le remplissage de votre objet peut **varier entre 0% et 35%**, **au-delà** de 35% vous ne gagnez plus en solidité, cela est considéré comme du **gaspillage de matière.**

   * Définissez le **motif de remplissage**, il joue également un rôle dans la solidité de votre objet. Vous pouvez choisir entre plusieurs motifs, chacun ayant des **géométries particulières.**

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/clementine.benyakhou/-/raw/master/docs/images/capt2.JPG)

4. _Jupe et bordure_ 

  * La **jupe** définit le périmètre de votre objet. C'est empreinte de votre objet. Elle permet de vérifier que votre objet ne joue pas trop avec les limites du plateau. 
  * La **bordure** permet de « solidifier », « asseoir » la base de votre objet. On réalise une large bordure quand l’objet prend une certaine hauteur sur le plateau. 

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/clementine.benyakhou/-/raw/master/docs/images/capt3.JPG)

5. _Supports_

  * Entant donné que les pièces réalisées en impression 3D sont construites couche par couche, une couche se superposant à l’autre sur laquelle s’appuyer est nécessaire. En fonction du modèle 3D que vous voulez imprimer, il est parfois obligatoire de faire des **supports**. Chaque couche imprimée comme un ensemble de filaments chauffés qui adhèrent aux filaments situés en dessous et autour. Chaque fil est imprimé légèrement décalé par rapport à sa couche précédente. Cela permet de construire un modèle jusqu'à des **angles de 45°,** ce qui permet aux couches de s'étendre au-delà de la largeur de la couche précédente. Lorsqu'un élément est **imprimé avec un angle de surplomb supérieur à 45°, il peut s'affaisser et nécessite un support en dessous pour le maintenir en place.** Dans ce cas, il suffit de cocher la case **« Générer des supports »,** cela permettra de palier les ponts. 

  * Certains supports ne sont pas indispensables vous pouvez cocher la case « Supports de plateau uniquement » Cette fonction ne générera que des supports sur le plateau. 

  * Si vous désirez un résultat net, il est parfois judicieux de jouer sur la largeur de l’interface des supports, le nombre d’interface, l’espacement des motifs de supports… C’est à vous de juger. 

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/clementine.benyakhou/-/raw/master/docs/images/capt4.jpg)

**ETAPE 3 : REGLAGE DU FILAMENT**

Le **choix du filament** est important. C’est le moment où vous choisissez la matière de votre impression. Il existe plusieurs filaments plastiques. Certains sont plus souples, plus flexibles, recyclés, composé de poussière de bois, … Le choix est varié. Votre choix se fera en fonction du résultat escompté et de la fonction que votre objet devra remplir (résistance au frottement, prototypage rapide, …) Comment renseigner ses informations dans le slicer ?

Il existe deux manières de procéder. Rendez-vous dans l’onglet « Réglages du filament »

1. _Méthode 1 :_ 

Vous pouvez directement **choisir votre filament dans une base de données répertoriée dans le slicer**. Cependant, certaines données ne sont pas toujours correctes et méritent le coup de vérifier ces informations en les comparants avec les informations indiquées sur la bobine de fil que vous allez utiliser. 


2. _Méthode 2 :_

Vous **encodez manuellement les données**. Cette méthode est la plus sure des deux.

1)	Vérifiez bien que le **diamètre de votre filament**. Pour les Prusa que nous utilisons, le diamètre est de **1.75mm.** Pour d’autres imprimantes, il existe des diamètres plus grands. 
2)	Vérifiez la **température de chauffe de l’extrudeur et du plateau.** Vous trouvez ces informations sur une des faces des bobines. Par exemple pour du PLA, la température d’extrusion oscille entre 200-215° et la température du plateau oscille entre 60°et 80°. 

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/theo.dellicourt/-/raw/master/docs/images/MODULE%203/prusareglage4.PNG)

**ETAPE 4 : GENERER SON G-CODE**


1. Retournez sur le **plateau** et cliquez sur le « bouton » **« Découper maintenant »**, vous devez attendre quelques secondes avant de pouvoir exporter votre G-code. 
2. Cliquez à nouveaux au même endroit pour **exporter votre G-Code**

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/maxime.wauthij/-/raw/master/docs/images/Module%203/impression-min.png)

Après avoir réalisé toutes ces étapes, il est temps de **lancer l’impression.** 

## Utilisation de la machine ? 

### IMPRESSION 3D : Comment lancer l’imprimante après avoir préparé son G-code ? 

1. Utilisez le G-code que vous venez de générer à l’aide du Slicer et **mettez-le sur une carte SD, dans un dossier à votre nom.** 
2. **Placez la carte SD** dans l’emplacement qui lui est destiné, il se trouve sur votre gauche. (La carte se glisse verso face à vous)
3. Un message s’affiche, vous pouvez **sélectionner le fichier** que vous voulez imprimer, cliquez sur la molette à votre droite pour sélectionner le fichier et/ou faites la rouler pour faire défiler les fichiers. 

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/tejhay.pinheiroalbia/-/raw/master/docs/images/dossier.jpg)

4. Une fois votre fichier sélectionné, la **buse et le plateau vont se mettre à température**. (Ces informations se trouvent sur l’écran LCD. Sur l’écran vous pouvez également observer, le temps d’impression qu’il vous reste) Arrivée à température, l’imprimante va se mettre en route, faire une calibration pour vérifier que le plateau est à l’horizontal et se lancer. 

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/tejhay.pinheiroalbia/-/raw/master/docs/images/chauffe.jpg)

5. **Recommandations : Nous vous demandons de rester le temps d’impressions des trois premières couches avant de pouvoir vaquer à vos occupations.** Durant le temps d’impression, il est important de venir jeter un coup d’œil de temps à autre pour s’assurer qu’il **reste assez de fil sur la bobine, que le fil s’extrude de manière continue, que votre pièce ne se décroche pas à un moment, …** Cette vérification est nécessaire et permet de ne **pas abimer ou bien de ne pas casser la machine.**



## Précaution à prendre : 

* **Savoir si ce que nous voulons imprimer nécéssite l'utilisation de l'imprimante 3D** et que c'est la meilleure méthode pour réaliser son "objet".

Exemples de choses à ne pas imprimer : **boites** (peuvent être réalisées plus facilement et plus rapidement avec la découpeuse laser), imprimer une **bobine**,... 


_Note : les images utilisées pour ce tuto ont été réalisées par les étudiants du cours Fabzéro Design 2020-2021._ 


